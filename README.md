# SERENITY

## Branch

| Nome Branch | Titolo | Note | Presente nella Demo | 
| ---------- | --------- | ------- | ------- |
| [Math](https://gitlab.com/paolino625/serenity/-/tree/Math) | Serenity con JUnit | Test Base Serenity. | Sì | 
| [FrequentFlyer](https://gitlab.com/paolino625/serenity/-/tree/FrequentFlyer)| Serenity con JUnit | Test Base Serenity. | No |
| [webTesting](https://gitlab.com/paolino625/serenity/-/tree/webTesting) | WebTesting con Serenity (WebDriver) | Duplice versione: con e senza Page Object. | Sì |
| [screenplayPattern](https://gitlab.com/paolino625/serenity/-/tree/screenplayPattern) | ScreenPlay Pattern con JUnit | Presente anche il confronto con PageObject. | Sì |
| [screenplayPatternCucumber](https://gitlab.com/paolino625/serenity/-/tree/screenplayPatternCucumber) | ScreenPlay Pattern con Cucumber | | No |

## Slide

Presenti nel branch master.